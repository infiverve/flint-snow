# begin
@log.trace("Started executing 'flint-snow:change:create_complex_change.rb' flintbit...")
begin
    # Flintbit Input Parameters
    @connector_name = @input.get('connector_name') # Name of the ServiceNow Connector
    if @connector_name.nil?
       @connector_name = @config.global('cl_serviceNow.complex_change.connector_name')
      if @connector_name.nil?
       @connector_name = 'servicenow'
    end
   end
    @action = 'create-record'                     # Contains the name of the operation: list
    @tableName = 'change_request'
    @data = {}
    
    # Common field for all the change request 
    @data['short_description'] = @input.get('short_description')
    if @input.get('description')
    @data['description'] = @input.get('description')
    end
    if @input.get('u_application_server_device')
    @data['u_application_server_device'] = @input.get('u_application_server_device')
    end
    @data['category'] = @input.get('category')
    @data['u_subcategory'] = @input.get('u_subcategory')
    @data['u_environment'] = @input.get('u_environment')
    if @input.get('u_dependent_tracks')
    @data['u_dependent_tracks'] = @input.get('u_dependent_tracks')
    end
    @data['requested_by_date'] = @input.get("requested_by_date") # 2017-08-31 06:13:26
    @data['u_request_by_end_date'] =@input.get('u_request_by_end_date')  # 2017-08-31 06:13:22
    
    # Hidden fields for all the change request
    if(@input.get('requested_by') != nil)
	@data['requested_by'] = @input.get('requested_by')
    else
       @data['requested_by'] = @config.global('cl_serviceNow.complex_change.requested_by')
    end
    
     if(@input.get('impact') != nil)
	@data['impact'] = @input.get('impact')
    else
       @data['impact'] = @config.global('cl_serviceNow.complex_change.impact')
    end
     if(@input.get('urgency') != nil)
	@data['urgency'] = @input.get('urgency')
    else
       @data['urgency'] = @config.global('cl_serviceNow.complex_change.urgency')
    end
    if(@input.get('priority') != nil)
	@data['priority'] = @input.get('priority')
    else
       @data['priority'] = @config.global('cl_serviceNow.complex_change.priority')
    end
    if(@input.get('risk') != nil)
	@data['risk'] = @input.get('risk')
    else
       @data['risk'] = @config.global('cl_serviceNow.complex_change.risk')
    end
    if(@input.get('type') != nil)
	@data['type'] = @input.get('type')
    else
       @data['type'] = @config.global('cl_serviceNow.complex_change.type')
    end
    if(@input.get('state') != nil)
	@data['state'] = @input.get('state')
    else
       @data['state'] = @config.global('cl_serviceNow.complex_change.state')
    end
    if(@input.get('assignment_group') != nil)
	@data['assignment_group'] = @input.get('assignment_group')
    else
       @data['assignment_group'] = @config.global('cl_serviceNow.complex_change.assignment_group')
    end
    
    # field for latent and emergency change
    if @input.get('justification')
    @data['justification'] = @input.get('justification')
    end
    # if @input.get('u_incident_number')
    #@data['u_incident_number'] = @input.get('u_incident_number')
    #end
    
    # field for latent change
    if(@input.get('work_start') != nil)
    @data['work_start'] = @input.get("work_start") # 2017-08-31 06:13:26
    end

    # field for urgency change request
    if(@input.get('u_urgent') != nil)
         if(@input.get('u_urgent') == 'true')
	     @data['u_urgent'] = true
         else
             @data['u_urgent'] = false
         end
    end
    @uploaded_files = @input.get('fileupload')
    @log.info"-------------uploaded_files DETAILS------- #{@uploaded_files}"
    if @input.get('sysparm_display_value')
    @sysparm_display_value = @input.get('sysparm_display_value')
    
    else
    @sysparm_display_value = true
    end
    
    @log.info("Flintbit input parameters are, connector name :: #{@connector_name} |action :: #{@action}| tableName :: #{@tableName} | data :: #{@data}")

    response = @call.connector(@connector_name)
                  .set('action', @action)
                  .set('table-name', @tableName)
                  .set('data', @data)
                  .set('sysparm_display_value', @sysparm_display_value)
                  .sync

    # ServiceNow Connector Response Meta Parameters
    response_exitcode = response.exitcode           # Exit status code
    response_message = response.message             # Execution status message

    # ServiceNow Connector Response Parameters
    response_body = response.get('body')

    if response_exitcode == 0
        result = @util.json(response_body)

        @log.info("Success in executing serviceNow Connector, where exitcode :: #{response_exitcode} | message :: #{response_message}")
        response = @call.bit("flint-snow:change:get_process_data.rb").setraw(response_body).sync
        #@log.info("============= RESPONSE DATA =============  #{response.get('data')}")
        @job_id = @input.jobid()
        #@data = response.get('data')
        @data["jobid"] =  @job_id
        @user_message = """**Change request created successfully.**
        
	* Number : #{response.get('data').get('number')}
	* Short Description : #{response.get('data').get('short_description')}
	* Description : #{response.get('data').get('description').gsub!(/\s+/, ' ')}
	* Priority : #{response.get('data').get('priority')}
	* Risk : #{response.get('data').get('risk')}"""
        @output.setraw('result', "#{response}").set('exit-code', 0).set('user_message',@user_message).set('body', @data.to_s)
	#sleep (80000);
        #@call.bit("flint-snow:change:servicenowurl.rb").set("connector_name", "http").set('body', @data.to_s).sync
	
	if @uploaded_files != nil
          @uploaded_files.each do |file|
            
            @call.bit("flint-snow:change:attachment_upload.rb").set("connector_name", "servicenow").set("sys-id", response.get('data').get('sys_id')).set("file-name", file).sync
	  end
        end
	
        @log.trace("Finished executing 'serviceNow' flintbit with success...")
    else
        @log.error("Failure in executing serviceNow Connector where, exitcode :: #{response_exitcode} | message :: #{response_message}")
        @output.set('error', response_message).set('exit-code', 1)
        @log.trace("Finished executing 'serviceNow' flintbit with error...")
    end
rescue Exception => e
    @log.error(e.message)
    @output.set('exit-code', 1).set('message', e.message)
end
@log.trace("Finished executing 'flint-snow:change:create_complex_change.rb' flintbit...")
# end
