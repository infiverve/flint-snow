@log.trace("Started executing 'flint-snow:incident:update_callback.rb' flintbit...")
begin
@log.info("Flintbit input parameters :: #{@input.raw}")

@connector_name = @input.get("connector_name")
if @connector_name.nil?
   @connector_name = @config.global("serviceNow.connector_name")
    if @connector_name.nil?
       @connector_name = 'servicenow'
    end
end
@tableName = 'incident'
@incident_number = @input.get("number")
@sys_id = @input.get("sys-id")
@sysparm_display_value = @input.get('sysparm_display_value')

@log.debug("Connector : #{@connector_name} | Incident number : #{@incident_number} | SysId : #{@sys_id}")

          response = @call.connector(@connector_name)
                          .set('action', 'find-record')
                          .set('table-name', @tableName)
                          .set('sys-id', @sys_id)
                          .set('sysparm_display_value', @sysparm_display_value)
                          .sync

    # ServiceNow Connector Response Meta Parameters
    response_exitcode = response.exitcode           # Exit status code
    response_message = response.message             # Execution status message

    # ServiceNow Connector Response Parameters
    response_body = response.get('body')            # Response body

    if response_exitcode == 0
    
     @log.info("Success in executing serviceNow Connector, where exitcode :: #{response_exitcode} | message :: #{response_message}") 
     @log.info("Incident details :: #{response_body}")
     @output.setraw("result", "#{response_body}")
    
    else
     @log.error("Failure in executing serviceNow Connector where, exitcode :: #{response_exitcode} | message :: #{response_message}")
     @output.set('error', response_message)
    end

rescue Exception => e
  @log.error("exception occured : #{e.message}")
  @output.set('exit-code', 1).set('message', e.message)
end
@log.trace("Finished executing 'flint-snow:incident:update_callback.rb' flintbit...")
#end
