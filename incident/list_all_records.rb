# begin
@log.trace("Started executing 'flint-snow:incident:list_all_records.rb' flintbit...")
begin
    # Flintbit Input Parameters
    @connector_name = @input.get('connector_name') # Name of the ServiceNow Connector
   if @connector_name.nil?
       @connector_name = @config.global("serviceNow.connector_name")
      if @connector_name.nil?
       @connector_name = 'servicenow'
    end
   end
    @action = 'get-records'                     # Contains the name of the operation: list
    # optional
    @tableName = 'incident'
    #@sysparm_limit  = @input.get('sysparm_limit')
    @sysparm_query =  @input.get('sysparm_query')
    @sysparm_display_value =@input.get('sysparm_display_value')

    @sysparm_limit  = @config.global("serviceNow.sysparm_limit")
    @sysparm_offset = 0
    @flag = true

    #@sysparm_fields = "sys_id, number, closed_at, priority, incident_state, state, assigned_to, category ,correlation_id, sys_updated_by, sys_updated_on ,caller_id, assignment_group, short_description, escalation,impact, opened_at,subcategory,urgency,description,business_service,sys_created_on,u_external_reference_id,resolved_at"
 

    for i in 0..10000

       if @flag
         @sysparm_offset = 0
         @flag = false
       else
         @sysparm_offset = @sysparm_offset+@sysparm_limit
         @log.info("|||||||||||| :: #{@sysparm_offset}")
       end

    @log.info("Flintbit input parameters are, connector name :: #{@connector_name} |action :: #{@action}| tableName :: #{@tableName}")

          response = @call.connector(@connector_name)
                          .set('action', @action)
                          .set('table-name', @tableName)
                          .set('sysparm_limit', @sysparm_limit)
                          .set('sysparm_display_value', @sysparm_display_value)
                          .set('sysparm_offset',@sysparm_offset)
                          .timeout(1000000)
                          .sync
    # ServiceNow Connector Response Meta Parameters
    response_exitcode = response.exitcode           # Exit status code
    response_message = response.message             # Execution status message

    # ServiceNow Connector Response Parameters
  response_body = response.get('body')# Response body   
  test =response.get('heders')
  count = test.fetch("X-Total-Count")
  counts = count[0].to_i
  @log.info("COUNTS :: #{counts.to_s}")   

  if counts <= @sysparm_offset
    @log.info("############### BREAKING THE LOOP ###################")
    @flag = true
    break
   end

    if response_exitcode == 0
    	result = @util.json(response_body)
	
	 @log.info("Success in executing serviceNow Connector, where exitcode :: #{response_exitcode} | message :: #{response_message}")
         @call.bit("flint-snow:incident:process_data.rb").setraw(response_body).sync        
         @output.set("message", 'Success')
         @log.trace("Finished executing 'serviceNow' flintbit with success...")
    else
        @log.error("Failure in executing serviceNow Connector where, exitcode :: #{response_exitcode} | message :: #{response_message}")
        @output.set('error', response_message)
        @log.trace("Finished executing 'serviceNow' flintbit with error...")
    end
end
  
rescue Exception => e
    @log.error(e.message)
    @output.set('exit-code', 1).set('message', e.message)
end
@log.trace("Finished executing 'flint-snow:incident:list_all_records.rb' flintbit...")
# end
