# begin
@log.trace("Started executing 'flint-util:serviceNow:incident:get_commnets_and_worknotes.rb' flintbit...")
begin
    # Flintbit Input Parameters
    @connector_name = @input.get('connector_name') # Name of the ServiceNow Connector
     if @connector_name.nil?
       @connector_name = @config.global("serviceNow.connector_name")
      if @connector_name.nil?
       @connector_name = 'servicenow'
    end
   end
    @action = 'get-records'                     # Contains the name of the operation: list
    @sysparm_display_value =@input.get('sysparm_display_value')
    @sysparm_query = @input.get('sysparm_query')
    @sysparm_display_value =@input.get('sysparm_display_value')
    @sysid = @input.get('sys-id')
    @tableName = 'sys_journal_field'
    @sysparm_query = "element_id=#{@sysid}"  

    @log.info("Flintbit input parameters are, connector name :: #{@connector_name} |action :: #{@action}| tableName :: #{@tableName}")

          response = @call.connector(@connector_name)
                          .set('action', @action)
                          .set('table-name', @tableName)
                          .set('sysparm_limit', @sysparm_limit)
                          .set('sysparm_query', @sysparm_query)
                          .set('sysparm_display_value', @sysparm_display_value)
                          .sync
    # ServiceNow Connector Response Meta Parameters
    response_exitcode = response.exitcode           # Exit status code
    response_message = response.message             # Execution status message

    # ServiceNow Connector Response Parameters
    response_body = response.get('body')

    if response_exitcode == 0
	 @log.info("Success in executing serviceNow Connector, where exitcode :: #{response_exitcode} | message :: #{response_message}")
         @output.set('comment_and_work_notes', "#{response_body}")
    else
        @log.error("Failure in executing serviceNow Connector where, exitcode :: #{response_exitcode} | message :: #{response_message}")
        @output.set('comment and work-notes', response_message)
    end
rescue Exception => e
    @log.error(e.message)
    @output.set('exit-code', 1).set('message', e.message)
end
@log.trace("Finished executing 'flint-util:serviceNow:incident:get_commnets_and_worknotes.rb' flintbit...")
# end
