@log.info("Starting flintbit flint-snow:incident:process_data.rb")
data = @input.get("result")
      # @log.info("Data #{data}")
        
 	data.each do |data|   
        post_data = {}    
         @log.info("==============   #{data}")
        
        if ((!data["assignment_group"].nil?) && (!data["assignment_group"]["value"].nil?))
    	    post_data["assignment_group"] = data["assignment_group"]["value"]
            response = @call.bit("flint-snow:incident:list_assignmentgroup.rb").set("sys-id", post_data["assignment_group"]).sync
            if !response.nil?
                post_data["assignment_group"]=response.get("name")
            end	
        end

    
        if ((!data["assigned_to"].nil?) && (!data["assigned_to"]["value"].nil?))
            response = @call.bit("flint-snow:incident:list_assigned_to_user.rb").set("sys-id", data["assigned_to"]["value"]).sync
            if !response.nil?
            post_data["assigned_to"]=response.get("name")
      	end
        end

        if ((!data["caller_id"].nil?) && (!data["caller_id"]["value"].nil?))
        @log.info("CALLER :: #{data["caller_id"]}")
        post_data["caller_id"] = data["caller_id"]["value"]
        response = @call.bit("flint-snow:incident:list_caller_id.rb").set("sys-id", post_data["caller_id"]).sync
        response1 = response.to_s
        if !response.nil?
        if  !response1.include? "error" 
        caller_details = response.get("caller_details")
        caller_details1 = @util.json(caller_details)
        if !caller_details1.get("name").nil? 
        post_data["caller_name"]= caller_details1.get("name")
        end
        if !caller_details1.get("user_name").nil?
        post_data["caller_id"]= caller_details1.get("user_name")
        end
        end
	end
        end
        if ((!data["business_service"].nil?) && (!data["business_service"]["value"].nil?))
        post_data["business_service"] = data["business_service"]["value"]
        response = @call.bit("flint-snow:incident:list_business_service.rb").set("sys-id", post_data["business_service"]).sync
        if !response.nil?
        post_data["business_service"]=response.get("name")
       #@log.info("BUSINESS_SERVICE :: #{post_data["business_service"]}")
        end
        end
        if ((!data["location"].nil?) && (!data["location"]["value"].nil?))
        post_data["location"] = data["location"]["value"]
        #@log.info("LOCATION ::: #{data["location"]}")
        response = @call.bit("flint-snow:incident:list_location.rb").set("sys-id", post_data["location"]).sync
        post_data["location"]=response.get("name")
        end
        if ((!data["company"].nil?) && (!data["company"]["value"].nil?))
        post_data["company"] = data["company"]["value"]
        response = @call.bit("flint-snow:incident:list_company.rb").set("sys-id", post_data["company"]).sync
        post_data["company"]=response.get("name")
        end 

        post_data["priority"] = data["priority"]
        post_data["short_description"] = data["short_description"]
    	post_data["closed_at"] = data["closed_at"]
    	post_data["resolved_at"] = data["resolved_at"]
    	post_data["sys_updated_on"] = data["sys_updated_on"]
    	post_data["sys_created_on"] = data["sys_created_on"]
    	post_data["u_external_reference_id"] = data["u_external_reference_id"]
    	post_data["sys_id"] = data["sys_id"]
    	post_data["number"] = data["number"]
    	post_data["sys_updated_by"] = data["sys_updated_by"]
    	post_data["incident_state"] = data["incident_state"]
        post_data["state"] = data["state"]
    	post_data["correlation_id"] = data["correlation_id"]
    	post_data["category"] = data["category"] 
        post_data["subcategory"] = data["subcategory"]
	post_data["escalation"] = data["escalation"] 
        post_data["opened_at"] = data["opened_at"]
        post_data["description"] = data["description"] 
        post_data["impact"] = data["impact"] 
        post_data["urgency"] = data["urgency"] 
        post_data["contact_type"] = data["contact_type"]
        post_data["u_email_id"] = data["u_email_id"]
            


          @log.info("--------   Post Data #{post_data}")
          @call.bit("flint-snow:incident:servicenowurl.rb").set("connector_name", "http").set('body',post_data).sync

@log.info("End :: flint-snow:incident:process_data.rb")
      
=begin

	post_data["assignment_group"] = data["assignment_group"]["display_value"]
	post_data["assigned_to"] = data["assigned_to"]["display_value"]
	post_data["caller_id"] = data["caller_id"]["display_value"]
	post_data["short_description"] = data["short_description"]
	post_data["closed_at"] = data["closed_at"]
	post_data["sys_updated_on"] = data["sys_updated_on"]
	post_data["priority"] = data["priority"]
	post_data["sys_id"] = data["sys_id"]
	post_data["number"] = data["number"]
	post_data["sys_updated_by"] = data["sys_updated_by"]
	post_data["incident_state"] = data["incident_state"]
	post_data["correlation_id"] = data["correlation_id"]
	post_data["category"] = data["category"]
=end
	
        #@output.set("data", post_data)
        
       
end

